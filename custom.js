function onPageLoaded() {
    const input = document.querySelector('input[type=text]');
    const ul = document.querySelector('ul.todo_body');
    const addBtn = document.querySelector('.addBtn');
    const saveButton = document.querySelector('button.save');
    const clearButton = document.querySelector('button.clear');
    const addBlock = document.querySelector('.add_block');

    function createTodo() {
        const li = document.createElement('li');
        const textSpan = document.createElement('span');
        textSpan.classList.add('todo_text');
        const newTodo = input.value;
        textSpan.append(newTodo);

        const deleteBtn = document.createElement('span');
        deleteBtn.classList.add('todo_delete');
        deleteName='Delete';
        deleteBtn.innerHTML = deleteName;


        ul.appendChild(li).append(textSpan, deleteBtn);
        input.value = '';
        listenDeleteTodo(deleteBtn);
    }

    function listenDeleteTodo(element) {
        element.addEventListener('click', (event) => {
            element.parentElement.remove();
            event.stopPropagation();
        });
    }

     function onClickTodo(event) {
        if (event.target.tagName === 'LI') {
            event.target.classList.toggle('checked');
        }
    }

    addBtn.addEventListener('click', function(){
        if(input.value == ''){
            validTextEl = document.createElement('span');
            validText = 'Please, enter something there';
            validTextEl.innerHTML = validText;
            addBlock.appendChild(validTextEl);
        }else{
            createTodo();            
        }
    });
    input.addEventListener('click', function(){
        validTextEl.remove();
    });


    ul.addEventListener('click', onClickTodo);
   
    saveButton.addEventListener('click', function(){
        localStorage.setItem('todoList', ul.innerHTML);
    });
    clearButton.addEventListener('click', function(){
        ul.innerHTML = '';
        localStorage.removeItem('todoList', ul.innerHTML);
    });

    function loadTodos() {
        const data = localStorage.getItem('todoList');
        if (data) {
            ul.innerHTML = data;
        }
        const deleteButtons = document.querySelectorAll('span.todo_delete');
        for (const button of deleteButtons) {
            listenDeleteTodo(button);
        }
    }
    loadTodos();
}

document.addEventListener('DOMContentLoaded', onPageLoaded);